Modify the following items to configure iroiro.

**noteTypes**: the names of the note types that have pitch accent fields

If the note type of the card being edited does not have a note type in this list it will be ignored.

e.g. `"noteTypes": ["vocab"]`

**fields**: the names of fields to modify

Expects a list of pairs of field names where each pair is of the form `[sourceField, destinationField]`.

The vocab word is read from the source field in the pair and its pitch accent is written in the destination field.

For example if you want to read from the `word` field and write to the `accent` field:

```
"fields": [["word", "accent"]]`
```

For reading or writing to multiple fields:

```
fields": [["expression", "pitch-accent"], ["vocab-word", "pronunciation"]]
```


