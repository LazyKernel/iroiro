# iroiro

Plugin for pitch accent lookups in Anki.

## Building

Required assets for testing and building:

- A pronunciation database in `assets/pronunciation.csv`:
  - e.g. as distributed by the Japanese pronunciation addon https://ankiweb.net/shared/info/932119536
- For segmentation testing a copy of mecab and dictionary in `assets/support`
  - e.g. as distributed by the Japanese addon https://ankiweb.net/shared/info/3918629684 

Build a copy of the plugin in the `build` directory.

```
make build
```

Prepare a zipped package `iroiro.zip` to upload to ankiweb:

```
make package
```

Clear the build files:

```
make clean
```

Run the unit tests:

```
make test
```

### Example workflow

1. Create a sim link to the build directory

```
cd ~/.local/share/Anki2/addons21/
ln -s ~/src/iroiro/build iroiro
```

2. Make a build of project

```
cd ~/src/iroiro
make clean build
```

3. Open Anki and try using the plugin.

