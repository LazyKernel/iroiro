# pitch accent codes
# - used to represent the pitch of each character in a word
# 
# For example one of the pitch accents for 'タベモノ' can be represented as '0120'. We read this as:
#   1. 'タ' has low pitch
#   2. 'べ' has high pitch
#   3. 'モ' has high pitch but the following character will have low pitch
#   4. 'ノ' has low pitch
#
# The drop pitch code is used to distinguish heiban pitch accents from odaka pitch accents
LOW = "0" # indicates that the pitch is low
HIGH = "1" # indicates that the pitch is high
DROP = "2" # indicates that the pitch is high but the following character will drop

# pitch accents classifications
HEIBAN = 'heiban'
ATAMADAKA = 'atamadaka'
NAKADAKA = 'nakadaka'
ODAKA = 'odaka'

# accent classes
#  - used to indicate whether the pitch is high or low
HIGH_CLASS = "accent-high"
LOW_CLASS = "accent-low"

# breaks up a word into groups of characters based on whether they are low or high
#  - receives a tuple of the form (reading, accent_code)
#  - return a list of tuples of the form (grouped_characters, accent_class)
#  - e.g. pitch_classes(('タベモノ', '0120')) returns [('タ', LOW_CLASS), ('ベモ', HIGH_CLASS), ('ノ', LOW_CLASS)]
#
# assumes that a word:
# - can only rise once;
# - can only drop once; and
# - will rise before it drops
#
# there is no special representation for words that have a pitch drop immediately after the word (odaka pitch)
def pitch_classes(pronunciation):
    (reading, accent_code) = pronunciation

    # make flags to keep track of whether the pitch has already risen or fallen
    #   - if the first character is high then we pretend that we have already risen
    #   - NB: only DROP is a valid high accent code for the first character because atamadaka words drop immediately
    risen = accent_code[0] == DROP
    fallen = False

    # keep track of the words in the group so far
    collected = ''
    output = []
    for i, c in enumerate(reading):
        a = accent_code[i]
        # detect if the current character represents a rise
        if not risen and (a == HIGH or a == DROP):
            # mark the word as already risen
            risen = True
            # create a group for the collected words so far and clear the buffer
            #   - since we haven't risen yet it is safe to assume that this is the first low group in the word
            output.append((collected, LOW_CLASS))
            collected = ''
        # detect if the current character represents a drop
        elif risen and not fallen and a == LOW:
            # mark the word as already fallen
            fallen = True
            # create a group for the collected words so far and clear the buffer
            #   - this is the high group in the word since we have risen but not yet fallen
            output.append((collected, HIGH_CLASS))
            collected = ''
        collected += c
    # we have reached the end of the word and still have some characters in the collected buffer
    #   - if we have a heiban or odaka word then we have not yet fallen so the remaining characters should be high
    output.append((collected, LOW_CLASS if fallen else HIGH_CLASS))
    return output

# converts a pitch class tuple to its corresponding html markup
#  - receives a tuple of the form (group_of_characters, pitch_class)
#  - e.g. markup_for_pitch_class(('ソウ', HIGH_CLASS)) returns '<span class="high-pitch">ソウ</span>')
def markup_for_pitch_class(reading_and_pitch_class):
    (reading, pitch_class) = reading_and_pitch_class
    css_class = 'high-pitch' if pitch_class == HIGH_CLASS else 'low-pitch'
    return '<span class="{0}">{1}</span>'.format(css_class, reading)

# converts a word to html markup 
#  - it does this by rendering the markup for each pitch group and wraps the whole thing in a span to represent the accent type
#  - receives a list of tuples of the form (group_of_characters, pitch_class) and the accent type of the whole word
#  - e.g. pitch_accent_markup([('サ', HIGH_CLASS), ('キ', LOW_CLASS)], ATAMADAKA) returns '<span class="atamadaka"><span class="high-pitch">サ</span><span class="low-pitch">キ</span></span>')
def pitch_accent_markup(classes, accent_type):
    pitch_groups = ''.join([markup_for_pitch_class(pitch_class) for pitch_class in classes])
    return '<span class="{0}">{1}</span>'.format(accent_type, pitch_groups)

# classifies an accent_code to a pitch accent type
#  - e.g. accent_type('0120') returns NAKADAKA
#  - assumes that there is only one word represented by the accent_code
def accent_type(accent_code):
    first = accent_code[0]
    last = accent_code[-1]
    if first == DROP:
        return ATAMADAKA
    if last == LOW:
        return NAKADAKA
    if last == HIGH:
        return HEIBAN
    if last == DROP:
        return ODAKA

def remove_duplicates(input_list):
    output = []
    seen = set()
    for x in input_list:
        if not x in seen:
            output.append(x)
            seen.add(x)
    return output

def accent_types(accent_codes):
    return remove_duplicates([accent_type(code) for code in accent_codes])


# Converts a readings with their accent codes to the corresponding html markup
#   - receives a list of tuples (reading, accent_code)
#   e.g. markup_for_readings_and_accent_codes([('タベモノ', '0120')]) returns 
def markup_for_readings_and_accent_codes(readings_and_accent_codes):
    return remove_duplicates([pitch_accent_markup(pitch_classes((reading, accent_code)), accent_type(accent_code)) for reading, accent_code in readings_and_accent_codes])

# run unit tests
if __name__ == "__main__":
    def assertEqual(a, b):
        if not a == b: raise AssertionError("\n\n\nTest mismatch:\n      got: {0}\n expected: {1}\n\n\n".format(a, b))

    assertEqual(pitch_classes(('タベモノ', '0120')), [('タ', LOW_CLASS), ('ベモ', HIGH_CLASS), ('ノ', LOW_CLASS)])
    assertEqual(pitch_classes(('タベモノ', '0200')), [('タ', LOW_CLASS), ('ベ', HIGH_CLASS), ('モノ', LOW_CLASS)])
    assertEqual(pitch_classes(('アイサツ', '2000')), [('ア', HIGH_CLASS), ('イサツ', LOW_CLASS)])
    assertEqual(pitch_classes(('サキ', '20')), [('サ', HIGH_CLASS), ('キ', LOW_CLASS)])
    assertEqual(pitch_classes(('ウケタマワル', '011111')), [('ウ', LOW_CLASS), ('ケタマワル', HIGH_CLASS)])
    assertEqual(pitch_classes(('ワセン・リョーヨー', '200000111')), [('ワ', HIGH_CLASS), ('セン・リョーヨー', LOW_CLASS)])

    assertEqual(markup_for_pitch_class(('タ', LOW_CLASS)), '<span class="low-pitch">タ</span>')
    assertEqual(markup_for_pitch_class(('ベモ', HIGH_CLASS)), '<span class="high-pitch">ベモ</span>')

    assertEqual(pitch_accent_markup([('サ', HIGH_CLASS), ('キ', LOW_CLASS)], ATAMADAKA), '<span class="atamadaka"><span class="high-pitch">サ</span><span class="low-pitch">キ</span></span>')

    assertEqual(accent_type('0111'), HEIBAN)
    assertEqual(accent_type('2000'), ATAMADAKA)
    assertEqual(accent_type('0120'), NAKADAKA)
    assertEqual(accent_type('0112'), ODAKA)

    assertEqual(remove_duplicates([1, 2, 3, 3, 4, 3, 6]), [1, 2, 3, 4, 6])

    assertEqual(accent_types(['2000']), ([ATAMADAKA]))
    assertEqual(accent_types(['0120', '0200']), [NAKADAKA])
    assertEqual(accent_types(['20']), [ATAMADAKA])
    assertEqual(accent_types(['0112', '0111']), [ODAKA, HEIBAN])
    assertEqual(accent_types([]), [])

    assertEqual(markup_for_readings_and_accent_codes([]), [])
    assertEqual(
        markup_for_readings_and_accent_codes([('タベモノ', '0120'), ('タベモノ', '0200')]),
        [
            '<span class="nakadaka"><span class="low-pitch">タ</span><span class="high-pitch">ベモ</span><span class="low-pitch">ノ</span></span>',
            '<span class="nakadaka"><span class="low-pitch">タ</span><span class="high-pitch">ベ</span><span class="low-pitch">モノ</span></span>'
        ]
    )

    print("（＾ν＾）  all tests passed")
    quit()

