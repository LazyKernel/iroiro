## __init__.py is the entry point for the plugin
##   - responsible for integration with the anki runtime

import os

from .dictionary import build_dictionary, lookup_readings_and_accent_codes
from .accents import markup_for_readings_and_accent_codes
from .segmentation import lookup_segmentation

## if executing the file directly we are running the unit tests
#    - file resources are in the build directory
#    - the file is run from the src directory rather than the build directory
#    - build a relative path to modify imports so that they still work
#    - requires to run with __name__ as the src module to support relative imports
relative_path = os.path.join('..', 'build') if __name__ == "src" else ''
directory_path = os.path.dirname(os.path.normpath(__file__))

dictionary_path = os.path.join(directory_path, relative_path, 'db.csv')
card_stylesheet_path = os.path.join(directory_path, relative_path, 'card.css')

# read files from the disk into memory once on application start up
accent_dictionary = {}
with open(dictionary_path, encoding='utf-8') as csv_file:
    accent_dictionary = build_dictionary(csv_file)

card_stylesheet = ""
with open(card_stylesheet_path) as card_stylesheet_file:
    card_stylesheet = card_stylesheet_file.read()

def lookup_and_render_term(term, reading):
    readings_and_accent_codes = lookup_readings_and_accent_codes(accent_dictionary, term)
    # only show the readings that match the results from segmentation
    applicable_readings_and_accent_codes = filter(lambda found_reading_and_accent_code: found_reading_and_accent_code[0] == reading, readings_and_accent_codes)

    markups = markup_for_readings_and_accent_codes(readings_and_accent_codes)

    # if a pronunciation can't be found render a reading anyway
    #  - otherwise render all found readings for one term separated by a ／
    return "／".join(markups) if markups else reading

def lookup_and_render_sentence(sentence):
    if not sentence:
        return ""
    return "　".join([lookup_and_render_term(term, reading) for (term, reading) in lookup_segmentation(sentence)])

# set up a global config variable to initialised on startup
config = {
    'noteTypes': [],
    'fields': []
}

# to be executed when the user makes a change to a field
def add_accent(changed, note, f_index):
    # only modify the intended note type
    if note.model()['name'] not in config['noteTypes']:
        return changed
    for (sourceField, destinationField) in config['fields']:
        # do not overide existing fields
        if note[destinationField]:
            return changed

        rendered = lookup_and_render_sentence(note[sourceField])
        if rendered == "":
            return changed

        changed = True
        note[destinationField] = rendered
    return changed

# append css for pitch accent markers to the card before rendering it
def prepare_card(html, card, context):
    return "{0}\n<style type=\"text/css\">\n{1}</style>".format(html, card_stylesheet)

# add a button to the browse window to add pronunciations for the cards that are visible
def add_browse_action(browser):
    action = QAction("Bulk-add Pronunciations", browser)
    action.triggered.connect(lambda: add_accents_to_notes(browser.selectedNotes()))
    browser.form.menuEdit.addSeparator()
    browser.form.menuEdit.addAction(action)

def add_accents_to_notes(note_ids):
    for note_id in note_ids:
        note = mw.col.getNote(note_id)
        add_accent(False, note, 0)
        note.flush() 

if __name__ == "src":
    def assertEqual(a, b):
        if not a == b: raise AssertionError("\n\n\nTest mismatch:\n      got: {0}\n expected: {1}\n\n\n".format(a, b))

    assertEqual(lookup_and_render_term('挨拶', 'アイサツ'), '<span class="atamadaka"><span class="high-pitch">ア</span><span class="low-pitch">イサツ</span></span>')
    assertEqual(lookup_and_render_term('食べ物', 'タベモノ'), '<span class="nakadaka"><span class="low-pitch">タ</span><span class="high-pitch">ベモ</span><span class="low-pitch">ノ</span></span>／<span class="nakadaka"><span class="low-pitch">タ</span><span class="high-pitch">ベ</span><span class="low-pitch">モノ</span></span>')
    assertEqual(lookup_and_render_term('詐欺', 'サギ'), '<span class="atamadaka"><span class="high-pitch">サ</span><span class="low-pitch">キ</span></span>')
    assertEqual(lookup_and_render_term('明くる日', 'アクルヒ'), '<span class="odaka"><span class="low-pitch">ア</span><span class="high-pitch">クルヒ</span></span>／<span class="heiban"><span class="low-pitch">ア</span><span class="high-pitch">クルヒ</span></span>')
    assertEqual(lookup_and_render_term('unknown', 'unknown'), 'unknown')

    assertEqual(lookup_and_render_sentence(None), '')
    assertEqual(lookup_and_render_sentence(''), '')
    assertEqual(lookup_and_render_sentence('挨拶をする'), '<span class="atamadaka"><span class="high-pitch">ア</span><span class="low-pitch">イサツ</span></span>　ヲ　<span class="atamadaka"><span class="high-pitch">ス</span><span class="low-pitch">ル</span></span>／<span class="heiban"><span class="low-pitch">ス</span><span class="high-pitch">ル</span></span>')

    # prepare_card should append css to the provided html
    if 'hello' not in prepare_card("hello", '', ''): raise AssertionError("Expected to have css injected", test_html)
    if 'text/css' not in prepare_card("hello", '', ''): raise AssertionError("Expected to have css injected", test_html)

    from unittest.mock import MagicMock

    # add_accent should not populate other note types
    config['noteTypes'] = ['test-note']
    config['fields'] = [['Front', 'Back']]
    fields = { 'Front': '夏', 'Back': '' }
    note = MagicMock()
    note.model.return_value = {'name': 'other-note'}
    # allow dict style access on the mock note
    note.__getitem__.side_effect = fields.__getitem__
    note.__setitem__.side_effect = fields.__setitem__
    result = add_accent(False, note, '')
    assertEqual(note['Back'], '')
    assertEqual(result, False)

    # add_accent should add a pitch accent to the back of the card
    note.model.return_value = {'name': 'test-note'}
    result = add_accent(False, note, '')
    assertEqual(result, True)
    assertEqual(note['Back'], '<span class="odaka"><span class="low-pitch">ナ</span><span class="high-pitch">ツ</span></span>')

    # add accent should not modify existing pitch accents
    note['Front'] = '秋'
    result = add_accent(False, note, '')
    assertEqual(result, False)
    assertEqual(note['Back'], '<span class="odaka"><span class="low-pitch">ナ</span><span class="high-pitch">ツ</span></span>')

    print("(^^)/  all tests passed")

    ## running as main means that we only want to run the unit tests for this file
    #   - importing anki modules outside of the anki runtime will fail
    #   - so stop the execution of the programme early
    quit()

## import the old style anki tests because I am only running 2.1.15
#    - new style hooks were imported in 2.1.20
#    - docs https://addon-docs.ankiweb.net/hooks-and-filters.html
from anki.hooks import addHook
from aqt import mw
from PyQt5.QtWidgets import QAction

## bind hooks for anki addons
#   - hook definitions https://github.com/ankitects/anki/blob/c9574ec/qt/tools/genhooks_gui.py
addHook('editFocusLost', add_accent)
addHook('prepareQA', prepare_card)

## Load the config for the plugin
config = mw.addonManager.getConfig(__name__)

## Add an action to the browse actions menu
addHook("browser.setupMenus", add_browse_action)


