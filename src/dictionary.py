import csv

# csv columns
KANJI = 0
READING = 1
ACCENT_CODE = 2

def build_dictionary(csv_file):
    entries = {}
    reader = csv.reader(csv_file)
    for row in reader:
        if not row:
            continue
        existing = entries.setdefault(row[KANJI], [])
        existing.append((row[READING], row[ACCENT_CODE]))
    return entries

def lookup_readings_and_accent_codes(dictionary, term):
    return [] if not term in dictionary else dictionary[term]

# run unit tests
if __name__ == "__main__":
    import io

    def assertEqual(a, b):
        if not a == b: raise AssertionError("\n\n\nTest mismatch:\n      got: {0}\n expected: {1}\n\n\n".format(a, b))

    mock_csv = io.StringIO(
        """
挨拶,アイサツ,2000
食べ物,タベモノ,0120
食べ物,タベモノ,0200
詐欺,サキ,20
明くる日,アクルヒ,0112
明くる日,アクルヒ,0111
        """.strip()
    )

    mock_csv_empty_rows = io.StringIO(
        """
挨拶,アイサツ,2000

食べ物,タベモノ,0120

食べ物,タベモノ,0200

詐欺,サキ,20
明くる日,アクルヒ,0112

明くる日,アクルヒ,0111
        """.strip()
    )

    mock_dictionary = {
        '挨拶': [('アイサツ', '2000')],
        '食べ物': [('タベモノ', '0120'), ('タベモノ', '0200')],
        '詐欺': [('サキ', '20')],
        '明くる日': [('アクルヒ', '0112'), ('アクルヒ', '0111')]
    }

    assertEqual(build_dictionary(mock_csv), mock_dictionary)
    assertEqual(build_dictionary(mock_csv_empty_rows), mock_dictionary)

    assertEqual(lookup_readings_and_accent_codes(mock_dictionary, '挨拶'), [('アイサツ', '2000')])
    assertEqual(lookup_readings_and_accent_codes(mock_dictionary, '食べ物'), [('タベモノ', '0120'), ('タベモノ', '0200')])
    assertEqual(lookup_readings_and_accent_codes(mock_dictionary, '詐欺'), [('サキ', '20')])
    assertEqual(lookup_readings_and_accent_codes(mock_dictionary, '明くる日'), [('アクルヒ', '0112'), ('アクルヒ', '0111')])
    assertEqual(lookup_readings_and_accent_codes(mock_dictionary, 'unknown'), [])

    print("(. ❛ ᴗ ❛.)   all tests passed")
    quit()

