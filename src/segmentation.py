import glob
import os
import subprocess

directory_path = os.path.dirname(os.path.normpath(__file__))
addons_path = os.path.join(directory_path, '..')

mecab_path = None
addon_paths = os.scandir(addons_path)
for addon_path in addon_paths:
    if not addon_path.is_dir(): continue
    if not 'support' in os.listdir(addon_path): continue
    support_path = os.path.join(addon_path, 'support')
    if not 'mecab.lin' in os.listdir(support_path): continue
    mecab_path = support_path
    break

if not mecab_path:
    raise AssertionError("Could not find mecab, make sure you have Japanese support installed")

mecab_command = [
    os.path.join(mecab_path, 'mecab.lin'),
    '--node-format=%m~%f[8] ', # each node is of the format 今日~キョー with a space in between
    '--eos-format=\n',
    '--unk-format=%m[]',
    '-d',
    mecab_path,
    '-r',
    os.path.join(mecab_path, 'mecabrc')
]

# call mecab to split a sentence into words with their readings
#  - returns a list of tuples of the form (kanji_word, reading)
def lookup_segmentation(expression):
    mecab_process = subprocess.Popen(
        mecab_command,
        bufsize=-1,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        env={
            'DYLD_LIBRARY_PATH': mecab_path,
            'LD_LIBRARY_PATH': mecab_path
        }
    )
    stdin_data = expression.encode("utf-8") + b'\n'
    (stdout_data, stderr_data) = mecab_process.communicate(input=stdin_data, timeout=2)
    if stderr_data:
        raise RuntimeError("Error running mecab", stderr_data.decode('utf-8'))
    
    mecab_output = stdout_data.rstrip(b' \n').decode('utf-8')
    # parse the mecab output into pairs of kanji words with their pronunciations
    if mecab_output == "":
        return []

    output = []
    for segment in mecab_output.split(" "):
        if "~" in segment:
            # segment is of the form 腹~ハラ
            output.append(tuple(segment.split("~")))
        elif "[]" in segment:
            # segment is of the form ガヤガヤ[]
            term = segment.rstrip("[]")
            output.append((term, term))
    return output

# run unit tests
if __name__ == "__main__":
    def assertEqual(a, b):
        if not a == b: raise AssertionError("\n\n\nTest mismatch:\n      got: {0}\n expected: {1}\n\n\n".format(a, b))

    assertEqual(
        lookup_segmentation('今日はいい天気ですね'),
        [('今日', 'キョー'), ('は', 'ワ'), ('いい', 'イイ'), ('天気', 'テンキ'), ('です', 'デス'), ('ね', 'ネ')]
    )

    assertEqual(
        lookup_segmentation('天気がいいから散歩しましょう'),
        [('天気', 'テンキ'), ('が', 'ガ'), ('いい', 'イイ'), ('から', 'カラ'), ('散歩', 'サンポ'), ('し', 'シ'), ('ましょ', 'マショ'), ('う', 'ウ')]
    )

    assertEqual(
        lookup_segmentation('腹減った'),
        [('腹', 'ハラ'), ('減っ', 'ヘッ'), ('た', 'タ')]
    )

    assertEqual(lookup_segmentation(''), [])
    assertEqual(lookup_segmentation('ガヤガヤ'), [('ガヤガヤ', 'ガヤガヤ')])
    assertEqual(lookup_segmentation('こんにちは'), [('こんにちは', 'コンニチワ')])

    print("(・∀ ・)  all tests passed")
    quit()

