# build the simplified database of pronunciations
#  - assumes that there is a copy of the source database in `assets/pronunciation.csv`
build/db.csv: assets/pronunciation.csv
	mkdir -p build
	python3 -X utf8 scripts/build_database.py < assets/pronunciation.csv > build/db.csv

# prepares the files required to run the plugin in the build folder
#   1. builds and copies the pronunciations database
#   2. copies the source files into the build folder
build: build/db.csv
	cp src/*.py build/
	cp assets/card.css build/
	cp assets/config.md build/
	cp assets/config.json build/

# compresses the contents of the build folder to a zip file
iroiro.zip:
	zip -jr iroiro.zip build

# remove the build folder and any zip artefacts
clean:
	rm -rf build iroiro.zip

# run the unit test by executing each file directly
#  - run as module src to support relative imports
test:
	python3 -m src src/__init__.py
	python3 src/accents.py
	python3 src/dictionary.py
	python3 src/segmentation.py

# rebuilds the project and packages it up into a zip file
package: build iroiro.zip

